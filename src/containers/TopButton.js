import React from 'react';

const TopButton = props => {
    return (
        <a href='#topNavBar' id='scroll-to-top' className='dmtop global-radius'><i className='fa fa-angle-up'></i></a>
    )
};

export default TopButton;