import React from 'react';

import { Link } from 'react-router-dom';

const HeaderContainer = props => {
    return (
        <header className='top-navbar' id="topNavBar">
            <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                <div className='container-fluid'>
                    <Link className='navbar-brand' to='/'>
                        <img src='images/logo_bambino.png'
                            alt='Logotipo da Escola Bambino Camponês.'
                            style={{ margin: '5px 0px', maxHeight: '90px', maxWidth: '350px' }} />
                    </Link>
                    <button className='navbar-toggler'
                        type='button'
                        data-toggle='collapse'
                        data-target='#navbars-host'
                        aria-controls='navbars-rs-food'
                        aria-expanded='false'
                        aria-label='Toggle navigation'>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                    </button>
                    <div className='collapse navbar-collapse' id='navbars-host'>
                        <ul className='navbar-nav ml-auto'>
                            <li className='nav-item active'>
                                <Link className='nav-link' to='/'>Home</Link></li>
                            <li className='nav-item dropdown'>
                                <Link className='nav-link dropdown-toggle' to='#' id='dropdown-a'
                                    data-toggle='dropdown'>Escola </Link>
                                <div className='dropdown-menu' aria-labelledby='dropdown-a'>
                                    <Link className='dropdown-item' to='filosofia-da-escola'>Filosofia </Link>
                                    <Link className='dropdown-item' to='course-grid-3.html'>Nosssa história </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Nossa equipe </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Instalações </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Horários </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Depoímentos </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Nossa oração </Link>
                                </div>
                            </li>
                            <li className='nav-item dropdown'>
                                <Link className='nav-link dropdown-toggle' to='#' id='dropdown-a'
                                    data-toggle='dropdown'>Secretaria </Link>
                                <div className='dropdown-menu' aria-labelledby='dropdown-a'>
                                    <Link className='dropdown-item' to='course-grid-2.html'>Matrículas </Link>
                                    <Link className='dropdown-item' to='course-grid-3.html'>Material escolar </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Uniforme </Link>
                                    <Link className='dropdown-item' to='course-grid-4.html'>Determinações da escola </Link>
                                </div>
                            </li>
                            <li className='nav-item dropdown'>
                                <Link className='nav-link dropdown-toggle' to='#' id='dropdown-a' data-toggle='dropdown'>Mural
                                </Link>
                                <div className='dropdown-menu' aria-labelledby='dropdown-a'>
                                    <Link className='dropdown-item' to='blog.html'>Cardápio </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Avisos importantes </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Notícias </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Eventos </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Dicas </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Galerias de fotos </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Galerias de vídeos </Link>
                                </div>
                            </li>
                            <li className='nav-item dropdown'>
                                <Link className='nav-link dropdown-toggle' to='#' id='dropdown-a' data-toggle='dropdown'>Serviços
                                </Link>
                                <div className='dropdown-menu' aria-labelledby='dropdown-a'>
                                    <Link className='dropdown-item' to='blog.html'>Berçário I </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Berçário II </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Maternal I </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Maternal II </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Jardim I </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Jardim II </Link>
                                    <Link className='dropdown-item' to='blog-single.html'>Atividades extras </Link>
                                </div>
                            </li>
                            <li className='nav-item'><Link className='nav-link' to='contact.html'>Contato</Link></li>
                        </ul>
                        <ul className='nav navbar-nav navbar-right'>
                            <li><Link className='hover-btn-new log orange' to='#' data-toggle='modal'
                                data-target='#login'><span>Login</span></Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    )
};

export default HeaderContainer;