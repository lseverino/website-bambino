import React from 'react';
import { Link } from 'react-router-dom';

const CopyRightContainer = props => {
    return (
        <div className='copyrights'>
        <div className='container'>
            <div className='footer-distributed'>
                <div className='footer-center'>
                    <p className='footer-company-name'>Todos os direitos reservados. &copy; 2020 <Link to='#'>Bambino Camponês</Link> Design By :
                        <Link to='https://html.design/'>Maxigênios - Soluções em Tecnologia.</Link></p>
                </div>
            </div>
        </div>
    </div>
    
    )
};

export default CopyRightContainer;