import React, { useEffect, useState }  from 'react'

import api from '../services/api';

const CarrouselContainer = props => {

    const [banners, setBanners] = useState([]);

    useEffect( () => {
        api.get('/banners')
        .then(response => {
            console.log("Banners Retornados do Backend", response.data.banners);
            setBanners(response.data.banners);
        });
    }, [1] ); 

    return (
        <div id='carouselExampleControls' className='carousel slide bs-slider box-slider' data-ride='carousel'
            data-pause='hover' data-interval='false'>
            <ol className='carousel-indicators'>
                <li data-target='#carouselExampleControls' data-slide-to='0' className='active'></li>
                <li data-target='#carouselExampleControls' data-slide-to='1'></li>
                <li data-target='#carouselExampleControls' data-slide-to='2'></li>
            </ol>
            <div className='carousel-inner' role='listbox'>
                { banners.map((banner, index) => (
                    <div key={banner.id} className={`carousel-item ${index == 0 ? 'active' : ''}`}>
                        <div id='home' className='first-section'
                            style={{backgroundImage: "url('http://www.bambinocampones.com.br/media/" + banner.uploaded_resource + "')"}}>
                            {/*
                            <div className='dtab'>
                                <div className='container'>
                                    <div className='row'>
                                        <div className='col-md-12 col-sm-12 text-right'>
                                            <div className='big-tagline'>
                                                <h2><strong>SmartEDU </strong> education College</h2>
                                                <p className='lead'>With Landigoo responsive landing page template, you can promote
                                                    your all hosting, domain and email services. </p>
                                                <a href='#' className='hover-btn-new'><span>Contact Us</span></a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href='#' className='hover-btn-new'><span>Read More</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            */ }
                        </div>
                    </div>
                ))}
                
                <a className='new-effect carousel-control-prev' href='#carouselExampleControls' role='button' data-slide='prev'>
                    <span className='fa fa-angle-left' aria-hidden='true'></span>
                    <span className='sr-only'>Anterior</span>
                </a>
                <a className='new-effect carousel-control-next' href='#carouselExampleControls' role='button' data-slide='next'>
                    <span className='fa fa-angle-right' aria-hidden='true'></span>
                    <span className='sr-only'>Próximo</span>
                </a>
            </div>
        </div>
    )
};

export default CarrouselContainer;