import React from 'react';
import { Link } from 'react-router-dom';

const FooterContainer = props => {
    return (
        <footer className='footer'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-4 col-md-4 col-xs-12'>
                        <div className='widget clearfix'>
                            <div className='widget-title'>
                                <h3>Sobre nós</h3>
                            </div>
                            <p> Nossa escola atende há 19 anos, alunos da Educação Infantil de faixa etária dos 04 meses até aos 6 anos, pelos turnos da manhã e tarde. Temos como filosofia formar um cidadão crítico, participativo, solidário, comprometido com o contexto do país. Contamos com uma equipe de profissionais qualificados e material didático atualizado. Nosso objetivo é orientar, respeitando a individualidade de cada criança, como um cidadão em formação.</p>
                            <div className='footer-right'>
                                <ul className='footer-links-soi'>
                                    <li><Link to='#'><i className='fa fa-facebook'></i></Link></li>
                                    <li><Link to='#'><i className='fa fa-github'></i></Link></li>
                                    <li><Link to='#'><i className='fa fa-twitter'></i></Link></li>
                                    <li><Link to='#'><i className='fa fa-dribbble'></i></Link></li>
                                    <li><Link to='#'><i className='fa fa-pinterest'></i></Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-4 col-md-4 col-xs-12'>
                        <div className='widget clearfix'>
                            <div className='widget-title'>
                                <h3>Navegue direto</h3>
                            </div>
                            <ul className='footer-links'>
                                <li><Link to='#'>Home</Link></li>
                                <li><Link to='#'>Escola</Link></li>
                                <li><Link to='#'>Secretaria</Link></li>
                                <li><Link to='#'>Mural</Link></li>
                                <li><Link to='#'>Serviços</Link></li>
                                <li><Link to='#'>Contato</Link></li>
                            </ul>
                        </div>
                    </div>
                    <div className='col-lg-4 col-md-4 col-xs-12'>
                        <div className='widget clearfix'>
                            <div className='widget-title'>
                                <h3>Dados de Contato</h3>
                            </div>
                            <ul className='footer-links'>
                                <li><Link to='mailto:#'>direcao@bambinocampones.com.br</Link></li>
                                <li><Link to='#'>www.bambinocampones.com.br</Link></li>
                                <li>Rua Irmão Florêncio, nº 151 - Bairro Harmonia - Canoas - RS - Brasil - CEP: 92310-490</li>
                                <li>+55 (51) 3031-3240</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
};

export default FooterContainer;