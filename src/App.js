import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import ModalLogin from './containers/ModalLogin';
import HeaderContainer from './containers/HeaderContainer';
import FooterContainer from './containers/FooterContainer';
import CopyRightContainer from './containers/CopyRightContainer';
import TopButton from './containers/TopButton';

import IndexPage from './pages/IndexPage';
import FilosofiaEscolaPage from './pages/FilosofiaEscolaPage';

function App() {
  return (
    <BrowserRouter>
      <ModalLogin />
      <HeaderContainer />
      
      <Route path='/' exact component={IndexPage} />
      <Route path='/filosofia-da-escola' component={FilosofiaEscolaPage} />

      <FooterContainer />
      <CopyRightContainer />
      <TopButton />

    </BrowserRouter>
  );
}

export default App;
