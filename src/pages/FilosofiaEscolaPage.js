import React from 'react'

import TestimonialsContainer from '../containers/TestimonialsContainer';
import BooksIconsContainer from '../containers/BooksIconsContainer';


const FilosofiaEscolaPage = props => {

    return (
        <div className="App">
            <TestimonialsContainer />
            <BooksIconsContainer />
            
        </div>
    )
}

export default FilosofiaEscolaPage;