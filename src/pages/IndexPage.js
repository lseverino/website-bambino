import React from 'react'

import CarrouselContainer from '../containers/CarrouselContainer';
import NewsContainer from '../containers/NewsContainer';
import CountersContainer from '../containers/CountersContainer';
import ServicesContainer from '../containers/ServicesContainer';
import TestimonialsContainer from '../containers/TestimonialsContainer';
import BooksIconsContainer from '../containers/BooksIconsContainer';


const IndexPage = props => {

    return (
        <div className="App">
            
            <CarrouselContainer />
            <NewsContainer />
            <CountersContainer />
            <ServicesContainer />
            <TestimonialsContainer />
            <BooksIconsContainer />
            
        </div>
    )
}

export default IndexPage;